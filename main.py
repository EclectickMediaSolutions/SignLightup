from UserChecker import core
from log import logger, stream_handler

import serial
import argparse

SERIAL = serial.Serial('/dev/ttyUSB0', 9600)  # The serial object to write to


connected_members = {}

# DEBUG
core.CONNECTION_CONFIRM = 0
core.DISCONNECTION_CONFIRM = 300


def callback(person):
    """ Append `person` to `connected_members`, call `write_to_serial` with `count`. """
    connected_members[person.name] = person.is_connected

    count = 0

    for member in connected_members:
        if connected_members[member]:
            count += 1
        if not connected_members[member] and count >= 1:
            count -= 1

    write_to_serial(count)


def write_to_serial(value):
    """ If value is greater than 0, write on value to Arduino. Otherwise, output off. """
    logger.info('called write_to_serial with %s' % value)
    if value > 0:
        # if not parsed.quiet:
        logger.info('Wrote on value to Arduino')

        SERIAL.write(b'1')
    else:
        # if not parsed.quiet:
        logger.info('Wrote off value to Arduino')
        SERIAL.write(b'0')


def write_status(boolean):  # DEBUG.
    """ If boolean is true write status light (pin 13) on. """

    # The receiving Arduino equates 3 to True and 2 to False
    if boolean:  # If waiting for NMAP subprocess
        logger.info('Wrote status LED on')
        SERIAL.write(b'3')
    else:  # If not waiting for NMAP subprocess
        logger.info('Wrote status LED off')
        SERIAL.write(b'2')


parser = argparse.ArgumentParser()
parser.add_argument('-R', '--reset', help='Reset data for each person in DB',
                    action='store_true')

parser.add_argument('-q', '--quiet', help='Run the software, but don\'t output '
                    'to the screen.', action='store_true')

parsed = parser.parse_args()

if parsed.reset:
    core.reset(core.Loader().load())
    exit()

if parsed.quiet:
    logger.removeHandler(stream_handler)


if __name__ == '__main__':
    while 1:
        logger.info('Running NMAP Scan.')

        write_status(True)  # We will wait for NMAP subprocess
        core.generate_nmap(script_path='./macid_getter.sh').wait()
        write_status(False)  # We are not waiting for NMAP subprocess

        l = core.Loader()
        db = l.load()
        people = list(person for person in core.UserChecker(db, callback,
                                                            quiet=parsed.quiet))
        logger.info('Checked NMAP scan for %s people.' % len(people))
        logger.info('Refreshing DB')
        l.dump(db)
