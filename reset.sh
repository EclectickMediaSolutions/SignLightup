#! /bin/bash

ps -ax | grep "SignLightup" | grep -v "grep" |  cut -b 1-5 | xargs kill -9 &> /dev/null

echo "Running data reset"
python3 ~/Documents/SignLightup/main.py -R

echo "Restarting Software"
python3 ~/Documents/SignLightupe/main.py -q
