// PIN 13 for relay pin, doubles as relay status light with on board LED.
// PIN 7 is a status LED for NMAP scans. On equates to waiting for NMAP.
int relayPin = 11;
int statusLed = 7;  // 3 is on, 2 is off.

void setup() {
  Serial.begin(9600);
  pinMode(relayPin, OUTPUT);
  pinMode(statusLed, OUTPUT);
  pinMode(13, OUTPUT);
  digitalWrite(relayPin, HIGH);

}

void loop() {
  if (Serial.available() > 0) {
    char value = Serial.read();  // Get data from serial

    // Relay's switch logic voltages.
    if (value == '1') {
      digitalWrite(relayPin, LOW);  // Connect power to sign.
      digitalWrite(13, HIGH);
    } else if (value == '0') {
      digitalWrite(relayPin, HIGH);  // Disconnect power to sign.
      digitalWrite(13, LOW);
    } else if (value == '3') {  // Turn on  the NMAP status LED.
      digitalWrite(statusLed, HIGH);
    } else if (value == '2') {  // Turn off the NMAP status LED.
      digitalWrite(statusLed, LOW);
    } else {
      digitalWrite(relayPin, HIGH);
      digitalWrite(13, LOW);
    }
    Serial.println(value);
  }
  delay(1000);
}

