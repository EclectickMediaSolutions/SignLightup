- Power goes into the resistor, through the led, then out to ground. [1](1)

  - [Here](./lesson3sch2.jpg) is a wiring diagram.

  - Note, the resistor needs at least 100Ohms.

- The resistor needs a full 5v, so we don't have to worry about resistors for
  it.


[1]: ./led13bb.jpg
