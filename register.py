from UserChecker import register, data
import argparse
import os
import sys

try:
    if sys.argv[1].count('-l') or sys.argv[1].count('--list'):
        db = register.l.load()
        for person in db.people:
            print('%s | %s' % (person.name, person.ident))

        exit()

except IndexError:
    pass

parser = argparse.ArgumentParser()
parser.add_argument('name', help='The name of the user.')
parser.add_argument('identifier', help='The user\'s identifier.')
parser.add_argument('-l', '--list', help='Use this flag to list users '
                    'registered to the database.', action='store_true')

parsed = parser.parse_args()

if os.access('db.pkl', os.F_OK):
    register.register_user(register.l.load(), parsed.name, parsed.identifier)
else:
    register.register_user(data.Database(), parsed.name, parsed.identifier)
