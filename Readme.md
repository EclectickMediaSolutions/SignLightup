# Description

This module is intended to light up an open sign for [CrashBang Labs][cbl] whenever a user that has opted into the program (i.e registered with the database).

It depends on [UserChecker](https://github.com/EclectickMedia/UserChecker.git) to determine when a user has connected to the network and [PySerial](https://pypi.python.org/pypi/pyserial) to connect with an Arduino. For more information on how either module works, please refer to their respective documentation pages.

# Registering a user to the software:

> Note, this section uses software only implemented within the lab. Specifically `register.sh`

The software lives on the `Junk Server`, and should always restart on boot. The software itself is installed under the user account `arianagiroux`, and therefore cannot be directly accessed by the majority of users.

Due to the access limitations I have put in place, the best (read _easiest_) method to register a new user to the software is via a script located at `/home/crash/Desktop/sign_register.sh`.

## Running sign_register.sh

> To register properly you must know how to find your Mac ID. See the [iOS help][ios], [Android help][android], [MacOS help][macos], [Windows help][windows].

> On linux, run `ifconfig` from your terminal

1. Once you have logged in to the junk server, there should be the aforementioned `sign_register.sh`. Double click on the file, and select `run as script`.

2. The script should prompt you for a User Name and User Identifier. 

  1. The username can be specified as any reference identifier specified.
  (i.e `Ariana Giroux` or `Ariana's iPhone`.)

  2. The user identifier **MUST** be a valid `Mac ID`.

3. After entering user details, it will confirm that those details are correct and then register a new user to the sign software.

4. At this point, you can either repeat steps 1-3, or you can press `CTRL+C` to exit the software.

5. To enable the new changes, make sure that you restart the computer.

# Requirements

- [UserChecker](https://github.com/EclectickMedia/UserChecker.git)

   - Git already lists UserChecker, but requires that it be initialized once the repository has been cloned. This action is already defined in [install.sh](./install.sh).

- [PySerial](https://pypi.python.org/pypi/pyserial)

   - The full install information is listed in requirements.txt in a format that is parse able by `pip`. This action is already defined in [install.sh](./install.sh).


# Installation

For \*Nix users, installation of all dependencies is automatically handled by [install.sh](./install.sh).

> To register properly you must know how to find your Mac ID. See the [iOS help][ios], [Android help][android], [MacOS help][macos], [Windows help][windows].

> On linux, type `ifconfig` in your terminal.

[cbl]: www.crashbanglabs.org
[macid]: http://magma.maths.usyd.edu.au/images/faq/macos4.png
[ios]: https://help.utk.edu/kb/index2.php?func=show&e=2099
[android]: http://www.tomsguide.com/faq/id-2318718/locate-mac-address-android-smartphone.html
[macos]: http://www.iclarified.com/30929/how-to-find-your-mac-address-in-mac-os-x
[windows]: https://kb.wisc.edu/page.php?id=4273
